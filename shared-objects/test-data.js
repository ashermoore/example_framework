module.exports = {
    validLoginUsername: 'automationtest@testemail.com',
    validLoginPassword: '123456',
    devAccessPassword: 'Purple Lobster',
    environment: {
        local: 'https://localhost.airtasker.com',
        dev: 'https://dev.airtasker.com/',
        stage: 'https://stage.airtasker.com/',
        prod: 'https://www.airtasker.com/'
    }
};
