var path = require('path');

var requestHeaders;
var requestBody;

var self = module.exports = {
    /**
     * Gets the session cookie from the browser so it can be used to auth API calls
     * @param {String} callback - Returns the session token
     * @returns {String} which is used as x-auth-token in api call header
     * @example
     *      helperFunctions.getXauthSession(function(token) {
     *          console.log(token);
     *      });
     */
    getXauthSession: function(callback) {
        do {
            driver.sleep(100);
        }
        while (driver.manage().getCookie('session') === false);
        driver.manage().getCookie('session').then(function (cookie) {
            requestHeaders['x-auth-token'] = cookie.value;
            callback();
        });
    },

    loadAPIData: function(file, callback) {
        var fs = require('fs'),
            obj
        fs.readFile(file, handleFile)
        function handleFile(err, data) {
            if (err) throw err
            obj = JSON.parse(data);
            callback(obj);
        }
    },

    postTask: function() {
        self.loadAPIData(path.join(__dirname, '/API_Calls/post_a_task.json'), function(body) {
            requestHeaders = body["request"]["header"];
            requestBody = body["request"]["body"];

            requestHeaders.origin = 'https://dev.airtasker.com';
            requestHeaders.referer = 'https://dev.airtasker.com';
            self.getXauthSession();
            requestHeaders.authority = 'dev-api.airtasker.com';

            console.log(requestHeaders);
        });
    }
};
