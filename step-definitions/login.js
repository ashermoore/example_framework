const globalNav = require('../page-objects/globalNavigation');
const loginDialog = require('../page-objects/login');
const dashboard = require('../page-objects/dashboard');

module.exports = function () {

    this.Then(/^I click the navigation menu item "([^"]*)"$/, function (menuItem) {

        // click the login button at the top of the homepage
        return driver.findElement(By.css(globalNav.login)).click();
    });

    this.Then(/^I will confirm all elements of the login dialog$/, function () {

        // confirm all important parts of the login dialog
        for (let i = 0; i < loginDialog.length; i++) {
            const dialogItem = loginDialog[i];
            return driver.wait(until.elementLocated(By.css(dialogItem)), 5 * 1000);
        }
        return undefined;
    });

    this.When(/^I enter valid login details$/, function () {

        driver.wait(until.elementLocated(By.css(loginDialog.submitButton)), 5 * 1000);

        // use valid login details to log in
        return driver.findElement(By.css(loginDialog.emailInput)).sendKeys(shared.testData.validLoginUsername).then(function() {
            return driver.findElement(By.css(loginDialog.password)).sendKeys(shared.testData.validLoginPassword);
        });
    });

    this.Then(/^I will be logged in successfully$/, function () {

        // click the submit button and wait for dashboard to load
        return driver.findElement(By.css(loginDialog.submitButton)).click().then(function () {
            // store session cookie for API interactions
            driver.wait(until.elementLocated(By.css(dashboard.avatar)));
        });
    });

};
