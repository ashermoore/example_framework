const helper = require('../shared-objects/helperFunctions');

module.exports = function () {

    this.Then(/^I will create a new task$/, function () {
        helper.postTask();
        driver.sleep(5000);
    });

};