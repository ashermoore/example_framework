module.exports = {
    closeCross: 'div[class="close-cross"]',
    dialogHeader: 'div[class="dialog-panel-header"]',
    toggleLogin: 'a[id="toggle-login"]',
    facebookLogin: 'button[class="facebook-connect button-lrg center full-width"]',
    emailInput: 'input[id="email-input"]',
    password: 'input[id="password-input"]',
    submitButton: 'button[id="login-button"]',
    forgotPasswordLink: 'a[class="forgot-password-link small center margin-20-top"]'
};
