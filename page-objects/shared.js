module.exports = {
    gotoEnv: function (environment) {
        if(environment === 'local') {
            driver.navigate().to(shared.testData.environment.local);
        }

        if(environment === 'dev') {
            driver.navigate().to(shared.testData.environment.dev);
            driver.switchTo().alert().then(
                function (alert) {
                    alert.sendKeys(shared.testData.devAccessPassword);
                    alert.accept();
                });
        }

        if(environment === 'stage') {
            driver.navigate().to(shared.testData.environment.stage);
            driver.switchTo().alert().then(
                function (alert) {
                    alert.sendKeys(shared.testData.devAccessPassword);
                    alert.accept();
                });
        }

        if(environment === 'prod') {
            driver.navigate().to(shared.testData.environment.prod);
        }
    }
}