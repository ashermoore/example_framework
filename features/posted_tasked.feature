@tasker
Feature: Confirm elements on the view task screen

  Background:
    Given I go to the airtasker website
    When I click the navigation menu item "Login"
    Then I will confirm all elements of the login dialog
    When I enter valid login details
    Then I will be logged in successfully

  Scenario: Create task and confirm its elements
    Then I will create a new task
    And confirm all the important elements are present
