@login
Feature: Login to airtasker
  
  Scenario: Login to airtasker
    Given I go to the airtasker website
    When I click the navigation menu item "Login"
    Then I will confirm all elements of the login dialog
    When I enter valid login details
    Then I will be logged in successfully